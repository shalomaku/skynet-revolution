

import java.util.LinkedList;
import java.util.Scanner;

/***
 * 
 * @author sky
 * Network class
 */
class Network {
	private Node[] nodes;
	
	/**
	 * 
	 * @param N Network nodes
	 */
	public Network(int N) {
		initGraph(N);
	}
	
	/**
	 * Create node with value which represents index too
	 * 
	 * @throws ArrayIndexOutOfBoundsException if value (index) is greater than the network length
	 * @param value
	 */
	public void addNodeWithValue(int value) {
		if(value < 0 && value >= this.nodes.length) {
			throw new ArrayIndexOutOfBoundsException();
		}
		this.nodes[value] = new Node(value);
	}
	
	/**
	 * @param index
	 * @return Node
	 */
	public Node getNodeAt(int index) {
		if(index < 0 && index >= this.nodes.length) {
			throw new ArrayIndexOutOfBoundsException();
		}
		return this.nodes[index];
	}
	
	/**
	 * Return the length of node
	 * 
	 * @return int
	 */
	public int getNodesLength() {
		return this.nodes.length;
	}
	
	/**
	 * 
	 * @param N
	 */
	protected void initGraph(int N) {
		this.nodes = new Node[N];
		for(int i = 0 ; i < N; i++)
			this.addNodeWithValue(i);
	}
}


/**
 * 
 * @author sky
 * Node class
 */
class Node {
	/**
	 * The value of Node
	 */
	private int value;

	/**
	 * The list of adjacent nodes
	 */
	private LinkedList<Node> adjacentNodes;

	/**
	 * Constructor of node.
	 *
	 * @param val
	 *            the node value
	 */
	public Node(int val) {
		this.value = val;
		this.adjacentNodes = new LinkedList<Node>();
	}

	/**
	 * Add adjacent node
	 * 
	 * @param node
	 *            The node to add
	 */
	public void addAdjacentNode(Node node) {
		this.adjacentNodes.add(node);
	}
	
	/**
	 * 
	 * @return
	 */
	public Node[] getAdjacentsNode() {
		return this.adjacentNodes.toArray(new Node[this.adjacentNodes.size()]);
	}

	/**
	 * Getter of value
	 * 
	 * @return value of node
	 */
	public int getValue() {
		return this.value;
	}
	
	/**
	 * Remove link between current element and his adjacent node in parameters
	 * @param Node element
	 */
	public void removeAdjacentNode(Node element) {
	    this.adjacentNodes.remove(element);
	}
}

/**
 * 
 * @author sky
 * 
 * Path class
 */
class Path {
	LinkedList<Node> nodes;
	
	/**
	 * private Contructor
	 */
	private Path() {
		this.nodes = new LinkedList<>();
	}
	
	/**
	 * Add node on top of stack
	 * @param n
	 */
	private void push(Node n) {
		this.nodes.add(n);
	}
	
	/**
	 * 
	 * @return t
	 */
	public int length() {
		return nodes.size();
	}
	
	public static Path Bfs (Network n, int src, int end) {
		final int networkLength = n.getNodesLength();
		Boolean[] visited = new Boolean[n.getNodesLength()];
		int[] parent = new int[networkLength];
		final LinkedList<Node> queue = new LinkedList<>();
		
		//visited and parent array initialization
		for(int i = 0; i < networkLength; i++) {
			visited[i] = false ; //all visited node is set to false
			parent[i] = -1; //For all parent equal to -1 there is no path
		}
		
		//get first node
		Node currentNode, firstNode ;
		firstNode = currentNode = n.getNodeAt(src);	
		Boolean find = (end == currentNode.getValue());
		queue.push(currentNode);
		
		while ((queue.isEmpty() == false) && (find == false)) {
			currentNode = queue.pop();
			Node[] adjacents = currentNode.getAdjacentsNode();
			for (Node m : adjacents) {
				if(visited[m.getValue()] == false) {
					visited[m.getValue()] = true;
					parent[m.getValue()] = currentNode.getValue();
					queue.add(m);
				}
			}
			find = (end == currentNode.getValue());
		}
		
		Path sp = new Path();
		if(find) { //We reach to the 
			while(currentNode.getValue() != firstNode.getValue()) {
				sp.push(currentNode);
				currentNode = n.getNodeAt(parent[currentNode.getValue()]);
			}
		}
		
		return sp;
	}
	
	public int getLastEdge() {
		int value = -1;
		if(this.length() > 0) {
			value = this.nodes.get(this.length() - 1).getValue();
		}
		return value;
	}	
	
}

/**
 * Auto-generated code below aims at helping you parse
 * the standard input according to the problem statement.
 **/
class Player {

	private static Scanner in;

	public static void main(String args[]) {

        in = new Scanner(System.in);
        int N = in.nextInt(); // the total number of nodes in the level, including the gateways
        int L = in.nextInt(); // the number of links
        int E = in.nextInt(); // the number of exit gateways
        
        //Create graph
		Network network = new Network(N);

        for (int i = 0; i < L; i++) {
            int N1 = in.nextInt(); // N1 and N2 defines a link between these nodes
            int N2 = in.nextInt();
            network.getNodeAt(N1).addAdjacentNode(network.getNodeAt(N2));
			network.getNodeAt(N2).addAdjacentNode(network.getNodeAt(N1));
        }
        
        //Init gateways
  		Node[] gateways = new Node[E];
        for (int i = 0; i < E; i++) {
            int EI = in.nextInt(); // the index of a gateway node
            gateways[i] = network.getNodeAt(EI);
        }

        // game loop
        while (true) {
            int SI = in.nextInt(); // The index of the node on which the Skynet agent is positioned this turn
            int minDist = Integer.MAX_VALUE;
			Path nearestPath = null;
			
			Path[] shortestPaths = new Path[E];
			//Find the nearest path from the current agent position
			for (int i = 0; i < E; i++) {
				shortestPaths[i] = Path.Bfs(network, SI, gateways[i].getValue());
				int length = shortestPaths[i].length();
				if(length != 0 && length < minDist) {
					minDist = length;
					nearestPath = shortestPaths[i];
				}
			}
			
			if(nearestPath == null)
			{
				System.out.println("No path found");
			} else {
				Node edgeToRemove = network.getNodeAt(nearestPath.getLastEdge());
				System.out.println(SI + " " + nearestPath.getLastEdge());
				network.getNodeAt(SI).removeAdjacentNode(edgeToRemove);
			}
        }
    }
}